<?php

// Gets the JSON body sent alongside this request
$body = file_get_contents('php://input');

// For now add the contents of this payload to a file to make sure the webhook
// did correctly receive the data
$file = __DIR__ . 'webhook.txt';
file_put_contents($file, $body);
