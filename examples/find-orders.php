<?php

require('../vendor/autoload.php');

use ForzaRefurbished\Models\APIClient;

// Set access token underneath before testing...
$accessToken = null;
if ($accessToken == null) {
  die('Please provide your access token in the file: `find-orders.php`');
}

$client = new APIClient($accessToken);

// When providing a sandbox token (starting with a `T`). You'll always receive 4
// test orders. When removing the token above with your real token you'll receive
// your own orders
$orders = $client->findOrders();

// Note: You can also find a single order by it's ID. This will be called as following.
// its wrapped in an array, so you can view it in action in the tableview underneath
// uncomment the line underneath to make this happen

// $orders = array($client->findOrderByID('FORZ-1234'));

?>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">

<table class="table">
  <tr>
    <th>ID</th>
    <th>Status</th>
    <th>SKUs</th>
    <th>Shipping Address</th>
    <th>Tracking Codes</th>
    <th>Created At</th>
  </tr>
  <?php foreach ($orders as $order) { ?>
    <tr>
      <?php
        // Look into `/models/entities/Order.php` to view all available getter
        // functions. All the provided data is available as a getter
      ?>
      <td><?php echo $order->getID(); ?></td>
      <td><?php echo $order->getState(); ?></td>
      <td><?php echo join(', ', array_map(function($item) { return $item->getSKU(); }, $order->getItems())); ?></td>
      <td>
        <?php echo $order->getShippingAddress()->getStreet(); ?>
        <?php echo $order->getShippingAddress()->getHouseNumber(); ?>,
        <?php echo $order->getShippingAddress()->getZipCode(); ?>
        <?php echo $order->getShippingAddress()->getCity(); ?>
      </td>
      <td><?php echo join(', ', $order->getTrackingCodes()); ?></td>
      <td><?php echo $order->getCreatedAt(); ?></td>
    </tr>
  <?php } ?>
</table>
