<?php

require('../vendor/autoload.php');

use ForzaRefurbished\Models\APIClient;

use ForzaRefurbished\Models\Entities\OrderAddress;
use ForzaRefurbished\Models\Entities\OrderItem;
use ForzaRefurbished\Models\Entities\PostOrder;

// Set access token underneath before testing...
$accessToken = null;
if ($accessToken == null) {
  die('Please provide your access token in the file: `create-orders.php`');
}

$client = new APIClient($accessToken);

$shippingAddress = new OrderAddress();
$shippingAddress->setFirstname('Jan');
$shippingAddress->setLastname('Janssen');
$shippingAddress->setTelephone('123456789');
$shippingAddress->setZipCode('1234AB');
$shippingAddress->setHouseNumber('1');
$shippingAddress->setHouseNumberAddition('');
$shippingAddress->setStreet('Graaf Engelbrechtlaan');
$shippingAddress->setCity('Breda');
$shippingAddress->setCountry('NL');
$shippingAddress->setVAT('1');

$orderItem = new OrderItem();
$orderItem->setSKU(123);
$orderItem->setQuantity(3);

$orderItem = new OrderItem();
$orderItem->setSKU(167);
$orderItem->setQuantity(5);

$order = new PostOrder();
$order->setShippingAddress($shippingAddress);
$order->addOrderItem($orderItem);
$order->addOrderItem($orderItem);

$response = $client->createOrder($order);

?>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">

<h2><?php echo 'Order created with ID: ' . $response->data->id; ?></h2>
<?php echo $response->message; ?>
