<?php

require('../vendor/autoload.php');

use ForzaRefurbished\Models\APIClient;

// Set access token underneath before testing...
$accessToken = null;
if ($accessToken == null) {
  die('Please provide your access token in the file: `find-products.php`');
}

$client = new APIClient($accessToken);
$products = $client->findProducts();

?>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css" rel="stylesheet">

<table class="table">
  <tr>
    <th>Title</th>
    <th>Rating</th>
    <th>Stock</th>
    <th>Retail price</th>
    <th>Your price</th>
  </tr>
  <?php foreach ($products as $product) { ?>
    <tr>
      <?php
        // Look into `/models/entities/Product.php` to view all available getter
        // functions. All the provided data is available as a getter. When missing
        // data be sure to check out the `getAttributes` function
      ?>
      <td><?php echo $product->getTitle(); ?></td>
      <td><?php echo isset($product->getAttributes()->quality) ? $product->getAttributes()->quality : ''; ?></td>
      <td><?php echo $product->getStockCount(); ?></td>
      <td><?php echo $product->getRetailPriceExcludingVAT(); ?></td>
      <td><?php echo $product->getPriceExcludingVAT(); ?></td>
    </tr>
  <?php } ?>
</table>
