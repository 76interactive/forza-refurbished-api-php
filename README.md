# Forza Refurbished API wrapper in PHP
To help you getting started with our RESTful HTTP API, we've written a small PHP Library to show a basic implementation. You can directly use this library to interact with the API. This library provides a thin abstraction layer to make it possible to interact without writing any HTTP requests yourself. To view the complete API documentation please head over to: [the reseller documentation](http://api.forza-refurbished.nl/docs/#/Reseller).

### Getting started
The easiest way to include this repository in your project is by using composer. This project is not published to packagist so you need to configure the source yourself. Underneath is an example of how this will be done. Make sure to include HTTPful and the API in the require section of the composer.json file. You can also download this project and require `./vendor/autoload.php` to integrate it without using composer.    
    
Example composer.json file

```
{
  "name": "NAME_OF_YOUR_PROJECT",
  "description": "DESCRIPTION_OF_YOUR_PROJECT",
  "repositories": [{
    "type": "package",
    "package": {
      "name": "forza-refurbished/forza-refurbished-api-php",
      "version": "dev-master",
      "source": {
        "type": "git",
        "url": "https://bitbucket.org/76interactive/forza-refurbished-api-php.git",
        "reference": "master"
      },
      "autoload": {
        "psr-4": {
          "ForzaRefurbished\\": ""
        }
      }
    }
  }],
  "require": {
    "nategood/httpful": "*",
    "forza-refurbished/forza-refurbished-api-php": "dev-master"
  }
}
```

### Examples

In the `examples/` directory, you'll find various examples of how to use this package. Make sure to have a look over there.

### Functionalities

For now it's possible to:

* Find products
* Find orders
* Find a single order
* Create orders

### Support

Please send an email to [sjors.snoeren@forza-digital.com](mailto:sjors.snoeren@forza-digital.com) for any technical questions.