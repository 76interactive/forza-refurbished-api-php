<?php

namespace ForzaRefurbished\Models;

use ForzaRefurbished\Models\Entities\Product;
use ForzaRefurbished\Models\Entities\GetOrder;
use ForzaRefurbished\Models\Entities\PostOrder;

class APIException extends \Exception {
  private $error;
  private $statusCode;
  private $customMessage;

  public function __construct($error, $statusCode, $customMessage) {
    $this->error = $error;
    $this->statusCode = $statusCode;
    $this->customMessage = $customMessage;
  }

  public function __toString() {
    if (empty($this->customMessage)) {
      return $this->statusCode . ', ' . $this->error;
    }
    return $this->statusCode . ', ' . $this->error . ': ' . $this->customMessage;
  }
}

class APIClient {
  private $authKey;

  /**
  * The constructor, contains the API authorization key
  *
  * @param string $authKey
  */
  public function __construct($authKey) {
      $this->authKey = $authKey;
  }

  /**
  * The URL to return the API data from
  *
  * @param string $path
  */
  private function setupEndpoint($path) {
    return "http://api.forza-refurbished.nl/v1/" . $path;
  }

  /**
  * Compiling the headers to create a Get Request
  */
  private function createGetRequest($path) {
    $request = \Httpful\Request::get($this->setupEndpoint($path));
    $request->followRedirects(true);
    $request->addHeader('authorization', 'Bearer '.$this->authKey.'')
            ->sendsJson();
    return $request;
  }

  /**
  * Compiling the headers to create a Post Request
  */
  private function createPostRequest($path, $body) {
    $request = \Httpful\Request::post($this->setupEndpoint($path));
    $request->followRedirects(true);
    $request->addHeader('authorization', 'Bearer '.$this->authKey.'')
            ->body(json_encode($body))
            ->sendsJson();

    return $request;
  }

  /**
  * Completing the API request based on the request type
  */
  private function request($method = 'GET', $endpoint, $body = []) {
      if ($method === 'GET') {
        $this->request = $this->createGetRequest($endpoint);
      } else {
        $this->request = $this->createPostRequest($endpoint, $body);
      }
      $response = $this->request->send();

      if ($response->code >= 400) {
          $message = null;
          if (isset($response->body->message)) {
            $message = $response->body->message;
          }
          throw new APIException($response->body->error, $response->code, $message);

      } else {
          return $response->body;
      }
  }

  /**
  * Returns all the products
  */
  public function findProducts() {
      $response = $this->request('GET', 'products');
      $array = [];

      foreach ($response->data as $productJSON) {
          $products = new Product($productJSON);
          array_push($array, $products);
      }

      return $array;
  }

  /**
  * Returns all the orders
  */
  public function findOrders() {
      $response = $this->request('GET', 'orders');
      $array = [];

      foreach ($response->data as $orderJSON) {
        $orders = new GetOrder($orderJSON);
        array_push($array, $orders);
      }

      return $array;
  }

  /**
  * Return a single order based on the order ID
  *
  * @param string $id
  */
  public function findOrderByID($id) {
    return new GetOrder($this->request('GET', 'orders/' . $id)->data);
  }

  /**
  * Creates an order based on the address and order items passed in the order.
  */
  public function createOrder($order) {
    return $this->request('POST', 'orders', $order->toJSON());
  }
}
