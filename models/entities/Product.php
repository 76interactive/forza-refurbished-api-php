<?php

namespace ForzaRefurbished\Models\Entities;

class Product {
  private $sku;
  private $title;
  private $updatedAt;
  private $createdAt;
  private $attributes;
  private $defaultImage;
  private $images;
  private $retailPriceExcludingVAT;
  private $priceExcludingVAT;
  private $stockCount;

  public function __construct($json) {
    $this->sku = $json->sku;
    $this->title = $json->title;
    $this->updatedAt = $json->updatedAt;
    $this->createdAt = $json->createdAt;
    $this->attributes = $json->attributes;
    $this->images = (array)$json->images;


    $keyExists = array_key_exists('default', $this->images);
    if ($keyExists) {
      $this->defaultImage = $this->images['default'];
    }

    $this->retailPriceExcludingVAT = $json->pricing->retailExcludingVAT;
    $this->priceExcludingVAT = $json->pricing->excludingVAT;
    $this->stockCount = $json->stock->count;
  }

  /**
  * Returns the product SKU
  */
  public function getSku() {
    return $this->sku;
  }

  /**
  * Returns the product Title
  */
  public function getTitle() {
    return $this->title;
  }

  /**
  * Returns the date/time the product was updated
  */
  public function getUpdatedAt() {
    return new $this->updatedAt;
  }

  /**
  * Returns the date/time the product was created
  */
  public function getCreatedAt() {
    return $this->createdAt;
  }

  /**
  * Returns the attributes of the product
  */
  public function getAttributes() {
    return $this->attributes;
  }

  /**
  * Returns the product default image
  */
  public function getDefaultImage() {
    return $this->defaultImage;
  }

  /**
  * Returns the product images
  */
  public function getImages() {
    return $this->images;
  }

  /**
  * Returns the product retail price excluding VAT
  */
  public function getRetailPriceExcludingVat() {
    return $this->retailPriceExcludingVAT;
  }

  /**
  * Returns the product price excluding VAT
  */
  public function getPriceExcludingVat() {
    return $this->priceExcludingVAT;
  }

  /**
  * Returns the product stock count
  */
  public function getStockCount() {
    return $this->stockCount;
  }
}
