<?php

namespace ForzaRefurbished\Models\Entities;

use ForzaRefurbished\Models\JSONSerializable;

class PostOrder implements JSONSerializable {
  private $shippingAddress;
  private $items = [];

  public function setShippingAddress($shippingAddress) {
    $this->shippingAddress = $shippingAddress->toJSON();
  }

  public function addOrderItem($orderItem) {
    $this->orderItem[] = $orderItem->toJSON();
  }

  public function toJSON() {
    $orderItemsJSON = [];
    foreach($this->items as $orderItem) {
      $orderItemsJSON[] = $orderItem;
    }

    $orderData = array (
      'shipping' => [
        'address'=> $this->shippingAddress,
      ],
      'items' => $this->orderItem
    );
    return $orderData;
  }
}
