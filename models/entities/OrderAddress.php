<?php

namespace ForzaRefurbished\Models\Entities;

use ForzaRefurbished\Models\JSONSerializable;

/**
* Creates the order shipping address
*/
class OrderAddress implements JSONSerializable {
  private $firstname;
  private $lastname;
  private $telephone;
  private $zipCode;
  private $houseNumber;
  private $houseNumberAddition;
  private $street;
  private $city;
  private $countryCode;
  private $vatID;

  public function __construct($json = null) {
    if ($json == null) {
      return;
    }
    
    $this->firstname           = $json->firstname;
    $this->lastname            = $json->lastname;
    $this->telephone           = $json->telephone;
    $this->zipCode             = $json->zipCode;
    $this->houseNumber         = $json->houseNumber;
    $this->houseNumberAddition = $json->houseNumberAddition;
    $this->street              = $json->street;
    $this->city                = $json->city;
    $this->countryCode         = $json->countryCode;

    if (isset($json->vadId)) {
      $this->vatID = $json->vatId;
    }
  }

  public function getFirstname() {
    return $this->firstname;
  }

  public function getLastname() {
    return $this->lastname;
  }

  public function getTelephone() {
    return $this->telephone;
  }

  public function getZipCode() {
    return $this->zipCode;
  }

  public function getHouseNumber() {
    return $this->houseNumber;
  }

  public function getHouseNumberAddition() {
    return $this->houseNumberAddition;
  }

  public function getStreet() {
    return $this->street;
  }

  public function getCity() {
    return $this->city;
  }

  public function getCountryCode() {
    return $this->countryCode;
  }

  public function getVatID() {
    return $this->vatID;
  }

  /**
  * Sets the shipping address firstname
  *
  * @param string $firstname
  */
  public function setFirstname($firstname) {
    $this->firstname = $firstname;
  }

  /**
  * Sets the shipping address lastname
  *
  * @param string $lastname
  */
  public function setLastname($lastname) {
    $this->lastname = $lastname;
  }

  /**
  * Sets the shipping address telephone number
  *
  * @param string $telephone
  */
  public function setTelephone($telephone) {
    $this->telephone = $telephone;
  }

  /**
  * Sets the shipping address zipcode
  *
  * @param string $zipcode
  */
  public function setZipCode($zipCode) {
    $this->zipCode = $zipCode;
  }

  /**
  * Sets the shipping address house number
  *
  * @param string $houseNumber
  */
  public function setHouseNumber($houseNumber) {
    $this->houseNumber = $houseNumber;
  }

  /**
  * Sets the shipping address Additional house number
  *
  * @param string $houseNumberAddition
  */
  public function setHouseNumberAddition($houseNumberAddition) {
    $this->houseNumberAddition = $houseNumberAddition;
  }

  /**
  * Sets the shipping street address
  *
  * @param string $street
  */
  public function setStreet($street) {
    $this->street = $street;
  }

  /**
  * Sets the shipping city
  *
  * @param string $city
  */
  public function setCity($city) {
    $this->city = $city;
  }

  /**
  * Sets the shipping country code
  *
  * @param string $countryCode
  */
  public function setCountry($countryCode) {
    $this->countryCode = $countryCode;
  }

  /**
  * Sets the customer vat number
  *
  * @param string $vatId
  */
  public function setVAT($vatId) {
    $this->vatId = $vatId;
  }

  /**
  * Creates an array of all the shipping address data to be used
  */
  public function toJSON() {
    $shippingJson = [
      'firstname' => $this->firstname,
      'lastname' => $this->lastname,
      'telephone'=> $this->telephone,
      'zipCode' => $this->zipCode,
      'houseNumber' => $this->houseNumber,
      'houseNumberAddition' => $this->houseNumberAddition,
      'street' => $this->street,
      'city' => $this->city,
      'countryCode' => $this->countryCode,
      'vatId' => $this->vatId
    ];

    return $shippingJson;
  }
}
