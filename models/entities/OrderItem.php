<?php

namespace ForzaRefurbished\Models\Entities;

use ForzaRefurbished\Models\JSONSerializable;

/**
* Creates the order item 
*/
class OrderItem implements JSONSerializable {
  private $sku;
  private $quantity;

  public function __construct($json = null) {
    if ($json == null) {
      return;
    }

    $this->sku = $json->sku;
    $this->quantity = $json->quantity;
  }

  public function getSKU() {
    return $this->sku;
  }

  public function getQuantity() {
    return $this->quantity;
  }

  /**
  * Allows you to set the product sku of the order item
  *
  * @param string $sku
  */
  public function setSKU($sku) {
    $this->sku = $sku;
  }

  /**
  * Allows you to set the product quantity of the order item
  *
  * @param string $quantity
  */
  public function setQuantity($quantity) {
    $this->quantity = $quantity;
  }

  /**
  * Creates an array of the inputted data which is used to create the final order item
  */
  public function toJSON() {
    $itemJson = [
      'sku' => $this->sku,
      'quantity' => $this->quantity
    ];
    return $itemJson;
  }
}
