<?php

namespace ForzaRefurbished\Models\Entities;

class GetOrder {
  private $id;
  private $state;
  private $shippingAddress;
  private $items;
  private $trackingCodes;
  private $clientId;
  private $userId;
  private $updatedAt;
  private $createdAt;

  public function __construct($json) {
    $this->id = $json->id;
    $this->state = $json->state;
    $this->shippingAddress = $json->shipping;
    $this->items = $json->items;
    $this->trackingCodes = $json->trackingCodes;
    $this->clientId = $json->clientId;
    $this->userId = $json->userId;
    $this->updatedAt = $json->updatedAt;
    $this->createdAt = $json->createdAt;
  }

  /**
  * Returns the order IDs
  */
  public function getID() {
    return $this->id;
  }

  public function getState() {
    return $this->state;
  }

  /**
  * Returns the Shipping Addresses
  */
  public function getShippingAddress() {
    return new OrderAddress($this->shippingAddress->address);
  }

  /**
    * Returns the items in the order
    */
  public function getItems() {
    return array_map(function($item) {
      return new OrderItem($item);
    }, $this->items);
  }

  /**
  * Returns the order tracking codes
  */
  public function getTrackingCodes() {
    return $this->trackingCodes;
  }

  /**
  * Returns the client ID
  */
  public function getClientID() {
    return $this->clientId;
  }

  /**
  * Returns the User ID
  */
  public function getUserID() {
    return $this->userId;
  }

  /**
  * Returns the order updated Date/Time
  */
  public function getUpdatedAt() {
    return $this->updatedAt;
  }

  /**
  * Returns the order created Date/Time
  */
  public function getCreatedAt() {
    return $this->createdAt;
  }
}
