<?php

namespace ForzaRefurbished\Models;

/**
* Returns the inputted array as JSON
*/
interface JSONSerializable {
  function toJSON();
}
